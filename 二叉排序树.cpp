//二叉排序树
#include<iostream>
#include<stdlib.h>
using namespace std;
typedef int type;
typedef struct Node{
	type key;
	struct Node *left,*right;
	
}BiNode,*BiTree;
//二叉排序树插入
int insert(BiTree &root,type x){
	if(root==NULL){    //原树为空 直接插入根节点 
		root=(BiTree)malloc(sizeof(BiNode));	
		root->key=x;
		root->left=root->right=NULL;
		return 1;
	}
	else if(x==root->key)   //结点已经存在 直接返回失败 
		return 0;   
	else if(x<root->key)
		return insert(root->left,x);   //比根结点小 在左子树插入 
	else
		return insert(root->right,x);  //比根节点大 在右子树插入 
} 
//构造二叉排序树
void create(BiTree &root,type num[],int n){
	//root 是要构造的树，num是构造数据，n是数组长度
	root=NULL; 
	int i=0;
	while(i<n){
		insert(root,num[i]);   //插入函数 
		i++; 
	} 
	
} 
//查找结点是否存在 
int find(BiTree root,type x){
	if(root==NULL)	return -1;
	else if(root->key==x) return 1;
	else if(root->key>x) 
		return find(root->left,x);
	else
		return find(root->right,x);
	return -1; 
} 


//查找最小值

BiTree getMin(BiTree root) {

	if (root->left == NULL)
	{
		return root;
	}
	return getMin(root->left);
}

//查找最大值
BiTree getMax(BiTree root) {

	if (root->right == NULL)
	{
		return root;
	}
	return getMax(root->right);
}

//删除结点
BiTree BSTDelete(BiTree root, type k)
{
	//1.寻找待删除节点在链表中的位置,如果不存在该结点，p指针会一直向后移动直到最后一位
	BiTree  p,  f,  s,  q;
	p = root; f = NULL;
	while (p)
	{
		if (p->key == k)   break;
		f = p;//f为p的双亲结点
		if (p->key > k)
			p = p->left;
		else
			p = p->right;
	}
	//2.判断该点在二叉树中是否存在
	if (p == NULL) return root;
	//3.单分支结构
	if (p->left == NULL)//p无左子树
	{
		if (f == NULL)
			root = p->right;
		else  if (f->left == p)
			f->left = p->right;
		else
			f->right = p->right;
		free(p);
	}
	else//双分支结点
	{
		q = p; s = p->left;//此时将p赋值为q，s为p的左子树，则q为s的双亲
		while (s->right)//查找p的左子树中查找最右下结点——最大结点
		{
			q = s;
			s = s->right;//s为左子树最大结点，q为左子树最大节点的父母结点
		}
		if (q == p)//用来判断p结点的左子树是否有右子树的
			q->left = s->left;//s为p左子树的最右子树，所以它没有右子树了
		else
			q->right = s->left;
		p->key = s->key;
		free(s);
	}
	return  root;
}



//中序遍历函数
void inorder(BiTree root){
	if(root){
		inorder(root->left);
		cout<<root->key<<" ";
		inorder(root->right);
	}
	
} 
int main(){
	BiTree root;
	type num[]={8,7,4,3,1,2,3,4,9};
	create(root,num,9);
	inorder(root);
	cout<<endl;
	cout<<find(root,10)<<endl;
	cout << getMin(root)->key << endl;
	cout << getMax(root)->key << endl;
	root=BSTDelete(root, 3);
	inorder(root);
	cout << endl;
	return 0;
}
