//图 邻接矩阵
#include<iostream>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<queue>
#include<vector>
using namespace std;
#define MaxVerterNum 100   //顶点数目的最大值
typedef char vertextype;  //顶点数据类型
typedef int edgetype;    //边的数据类型
bool visited[MaxVerterNum]; //顶点是否访问的标记数组
typedef struct
{
	vertextype Vex[MaxVerterNum];    //顶点表    
	edgetype edge[MaxVerterNum][MaxVerterNum];   //邻接矩阵 边表 
	int vernum,arcnum;			//图的当前顶点数和边数 
 } graph;
//初始化图
void init(graph &g){
	
	g.vernum=0;
	g.arcnum=0;
} 
//创建图  
void create(graph &g){
	init(g);
	vertextype d;
	cout<<"输入顶点 输入 $结束"<<endl;
	cin>>d;
	while(d!='$')
	{
		g.Vex[++g.vernum]=d;   //顶点编号从1开始
		cin>>d;
	 } 
	 cout<<"输入边 输入 -1 -1结束"<<endl;
	 edgetype s1,s2;
	 cin>>s1>>s2;
	 while(s1!=-1&&s2!=-1){
	 	//构建邻接矩阵   现在创建的都是无向图
	 	g.edge[s1][s2]=1;
	 	g.edge[s2][s1]=1;
	 	g.arcnum++; 
	 	cin>>s1>>s2;
	 } 
	 
} 
// 判断边（x,y)是否存在
bool adjacent(graph g,edgetype x,edgetype y){
	 return g.edge[x][y]==1;
} 
//列出图中与结点 x邻接的边
void neighbors(graph g,vertextype num){
		//首先判断的顶点的存在 
		int i=0;
		for( i=0;i<g.vernum;i++)
			if(g.Vex[i]==num) break;
		
		//找到边
		for(int j=0;j<=g.vernum+1;j++) 
		{
			if(g.edge[i][j]==1) 
				cout<<i<<" "<<j<<endl; 
		}
		cout<<endl;
} 
//在图中插入顶点
void insertVertex(graph g, vertextype num) {
	g.Vex[++g.vernum] = num;
	//添加相应的边
	edgetype s1, s2;
	cout << "输入添加的边" << endl;
	cin >> s1 >> s2;
	while (s1 != -1 && s2 != -1)
	{
		g.edge[s1][s2] = 1;
	}
}
void visit(graph g, int num, vector<vertextype>& ans) { //访问顶点 就是将顶点加入到相应的 答案数组
	ans.push_back(g.Vex[num]);
}
// dfs
void dfs(graph g, int num, vector<vertextype>& ans)
{
	//从 num顶点开始深度优先遍历图 g
	
	visit(g, num, ans);
	visited[num] = true;
	for (int i = 1; i <= g.vernum; i++) {
		if (visited[i]==false && g.edge[num][i] == 1) {
			dfs(g, i,ans);
		}
	}
}

//bfs
void bfs(graph g,int num,vector<vertextype> &ans){
	//从 num顶点开始广度优先遍历图g
	//visited[num] = true;
	queue<int> q;
	q.push(num);
	visited[q.front()] = true;
	while (!q.empty()) {
		int w = q.front();
		q.pop();
		visit(g, w, ans);
		for (int i = 1; i <= g.vernum; i++)
		{
			if (visited[i] == false&&g.edge[w][i]==1) {
				 visited[i] = true;
				 q.push(i);
			}
		}
	}
}
int main(){
	graph g;
	create(g);
	//cout<<adjacent(g,16,2);
	//neighbors(g,'b');

	cout << "广度优先搜索：";
	memset(visited, 0, MaxVerterNum);
	vector<vertextype> ans1;
	for (int i = 1; i <= MaxVerterNum; i++) {
		if(!visited[i])
			bfs(g, i, ans1);
	}
	for (int i = 0; i < g.vernum; i++)
		cout << ans1[i] << " ";
	cout << endl;
	
	cout << "深度优先搜索：";
	vector<vertextype> ans;
	memset(visited, 0, MaxVerterNum);
	for (int i = 1; i <= g.vernum; i++) {
		if (!visited[i])
			dfs(g, i, ans);
	}
	for (auto a : ans)
		cout << a << " ";
	cout << endl;
	return 0;
}
//1 2
//2 1
//1 3
//3 1
//2 4
//4 2
//2 5
//5 2
//6 3
//3 6
//7 3
//3 7
//5 8
//8 5
//- 1 - 1
