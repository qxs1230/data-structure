//并查集
#include<iostream>
 
using namespace std;

/**
	（洛谷P1551）亲戚

	题目背景
	若某个家族人员过于庞大，要判断两个是否是亲戚，确实还很不容易，现在给出某个亲戚关系图，求任意给出的两个人是否具有亲戚关系。
	题目描述
	规定：x和y是亲戚，y和z是亲戚，那么x和z也是亲戚。如果x,y是亲戚，那么x的亲戚都是y的亲戚，y的亲戚也都是x的亲戚。
	输入格式
	第一行：三个整数n,m,p，（n<=5000,m<=5000,p<=5000），分别表示有n个人，m个亲戚关系，询问p对亲戚关系。
	以下m行：每行两个数Mi，Mj，1<=Mi，Mj<=N，表示Mi和Mj具有亲戚关系。
	接下来p行：每行两个数Pi，Pj，询问Pi和Pj是否具有亲戚关系。
	输出格式
	P行，每行一个’Yes’或’No’。表示第i个询问的答案为“具有”或“不具有”亲戚关系。
	
			测试数据
				6 5 3    	Yes
				1 2			Yes
				1 5			No
				3 4
				5 2
				1 3
				1 4
				2 3
				5 6 
				
				
				


**/
# define size 5005
int fa[size];
int rank[size];    //引入rank数组的作用主要是 在合并的时候 让深度小的父亲变为深度大的 
					//这样可以降低数的深度，减少find函数的复杂度 
//并查集初始化
void init(int n){
	 for(int i=0;i<n;i++)
	 		fa[i]=i,rank[i]=1;     //初始化 其指向自己 
} 
// 查询 也就是 查询其父亲是谁 

int find(int x)
{
    if(x == fa[x])   //自身就是根节点   
        return x;
    else{
        fa[x] = find(fa[x]);  //查找 其父亲的父亲 一直找到根节点为之 
        return fa[x];         //返回父节点
    }
}
//int find(int x)
//{
//    return x == fa[x] ? x : (fa[x] = find(fa[x]));
//}

// 合并操作
void merge(int i, int j)
{
    int x = find(i), y = find(j);    //先找到两个根节点
    if (rank[x] <= rank[y])
        fa[x] = y;
    else
        fa[y] = x;
    if (rank[x] == rank[y] && x != y)			
        rank[y]++;                   		//如果原来深度相同且根节点不同，则新的根节点的深度+1
} 

int main()
{
    int n, m, p, x, y;
    scanf("%d%d%d", &n, &m, &p);
    init(n);
    for (int i = 0; i < m; ++i)
    {
        scanf("%d%d", &x, &y);
        merge(x, y);
    }
    for (int i = 0; i < p; ++i)
    {
        scanf("%d%d", &x, &y);
        printf("%s\n", find(x) == find(y) ? "Yes" : "No");
    }
    return 0;
}



 
