// 元素位置从1 开始 即是 第一个元素的位置为 1 而数组的第一个元素下标为0 
#include<iostream>
#include<stdlib.h>
using namespace std;
#define maxSize 100
typedef int type; 
//线性表结构体 
typedef struct {
	type data[maxSize];
	type length;
} Node,*Sqlist; 

//检查线性表是否为空
bool isEmpty(Sqlist &L){
	return L->length<=0;
} 
//初始化线性表
void init(Sqlist &L){
	L->length=0;   
}    
//添加元素
void add(Sqlist &L,type x){
	//防止数组越界 
	if(L->length>=maxSize) return ;
	L->data[L->length++]=x;
} 
//在指定位置插入元素
int insertElem(Sqlist &L,int q,type x){
	//检查位置是否合法
	if(q<1||q>L->length+1||L->length==maxSize) return 0;
	//移动插入位置后面的元素
	for(type i=L->length;i>=q;i--)
		L->data[i]=L->data[i-1];
	//插入元素类型如果是字符型的话 需要特殊处理 x+'0'; 
	L->data[q-1]=x; 
	L->length++;
	return 1;
} 
//删除指定位置的元素
int deleteElem(Sqlist &L,int q){
	if(q<1||q>=L->length) return 0;
	for(type i=q;i<L->length;i++)
		L->data[i-1]=L->data[i];
	L->length--;
	return 0;
} 
//查找线性表中第一个值等于x的位置 
int findElem(Sqlist &L,type x){
	if(isEmpty(L)) return -1;
	for(type i=0;i<L->length;i++)
		if(x==L->data[i])
			return i+1;
	return 0;				
} 
//查找指定位置的元素
type getElem(Sqlist &L,int q){
	if(q<1||q>=L->length) 
		return -1;
	return L->data[q-1];
} 
//清空线性表 
void clearnSqlist(Sqlist &L){
	L->length=0;
	delete L; 
}
//打印线性表 
void print(Sqlist L){
	for(type i=0;i<L->length;i++)
		cout<<L->data[i]<<" ";
	cout<<endl;
}
//逆置顺序表
void reverse(Sqlist &L){
	for(int i=0;i<L->length/2;i++)
		swap(L->data[i],L->data[L->length-i-1]);
	
} 
//在时间复杂度为O(n) 空间复杂度为O（1）的要求下删除所有值为 x的元素
void delete_all_x(Sqlist &L,type x){
	if(isEmpty(L)) return ;
	int k=0;
	for(int i=0;i<L->length;i++)
		if(L->data[i]!=x){
			L->data[k]=L->data[i];
			k++;
		}
		L->length =k;
} 
//从有序顺序表中删除其值在给定值 s到t之间的（包含s 和t)所有元素
void delete_x_y(Sqlist &L,int s,int t){
	if(isEmpty(L)||s>t) return ;
	int l=findElem(L,s);
	int r=findElem(L,t);
	
	l--;
	r--;
	cout<<l<<" "<<r<<endl;
	if(l==-1||r==-1) return ;
	int h=1;
	for(int i=l;i<=r;i++)
		 L->data[i]=L->data[r+h],h++;
	L->length-=(r-l)+1;
} 
// 线性表循环左移 p个位置
void moveleft(Sqlist &L,int p){
	if(isEmpty(L)||p>L->length) return ;
	//把整个线性表分为前后两个部分
	//先逆转前面部分
	for(int i=0;i<p/2;i++)
		swap(L->data[i],L->data[p-i-1]);
	//再逆转后面一部分
	for(int i=p,j=0;i<p+((L->length-p)/2);i++,j++)
		swap(L->data[i],L->data[L->length-j-1]); 
	//再整体逆转
	print(L);
	reverse(L); 
} 
//查找两个升序序列的中位数
int midnum(Sqlist &L1,Sqlist &L2){
	 int len=L1->length+L2->length;
	 int mid=len/2;
	 int i=0,j=0,h=0;
	while(i<L1.length||j<L2.length){
		  
	}
} 
int main(){
	Sqlist L,L1;
	L=(Sqlist )malloc(sizeof(Node));
	L1=(Sqlist )malloc(sizeof(Node));
	init(L);
	init(L1);
	type x;
	cin>>x;
	while(x!=-1){
		add(L,x);
		cin>>x;
	}
	print(L);
	
	type y;
	cin>>y;
	while(y!=-1){
		add(L1,y);
		cin>>y;
	}	
	print(L1);
//	cout<<L->length<<endl; 
//	insertElem(L,1,10);
//	print(L);
//	cout<<L->length<<endl;
//	print(L);
//	deleteElem(L,4);
//	print(L);
//	cout<<findElem(L,4)<<endl;
//	cout<<getElem(L,2)<<endl;
//   	clearnSqlist(L);
//	cout<<L->length;
	//reverse(L);
	//print(L);
	//delete_all_x(L,2);
	//delete_x_y(L,2,5);
	//moveleft(L,4);
	
	print(L);
	
	
	return 0;
	
}


