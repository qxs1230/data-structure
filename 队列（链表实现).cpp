// 链表实现队列
#include<iostream>
#include<stdlib.h>
using namespace std;
typedef int type;
typedef struct LinkNode{
	type data;
	struct LinkNode *next;
}LinkNode;
typedef struct{
	LinkNode *front,*rear;
}LinkQueue;
//初始化
void init(LinkQueue &queue){
	queue.front=queue.rear=(LinkNode *)malloc(sizeof(LinkNode));
	queue.front->next=NULL;   //并不是 queue.front=NULL 
} 
//判断队列是否为空
bool isEmpty(LinkQueue queue){
	return queue.front==queue.rear;
} 
//入队
int  push(LinkQueue &queue,type x){ 
	LinkNode *s=(LinkNode *)malloc(sizeof(LinkNode));
	s->data=x;
	s->next=NULL;
	queue.rear->next=s;
	queue.rear=s; 
	if(queue.front==NULL) queue.front->next=s,queue.front=s;   //如果是都一个元素入队的时候 需要特殊处理 
	 
	return 1;
}
//出队
int pop(LinkQueue &queue){
	if(isEmpty(queue)) return -1;
	
	LinkNode *s=queue.front->next;
	queue.front->next=s->next;
	if(queue.rear==s)
		queue.rear=queue.front;
	type x=s->data;
	free(s);
	return x;
} 
//访问队顶元素
int top(LinkQueue &queue){
	if(isEmpty(queue)) return -1;
	return queue.front->next->data;
} 
int main(){
	LinkQueue queue;
	init(queue);
	type x;
	cin>>x;
	while(x!=-1){
		push(queue,x);
		cin>>x;
	}
	cout<<top(queue)<<endl;
	while(!isEmpty(queue))
		cout<<pop(queue)<<" ";
	cout<<endl;
	
	return 0;
}
