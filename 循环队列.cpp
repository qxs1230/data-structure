//循环队列  有一个单元空间没使用 用来标志标志队满  
#include<iostream>
using namespace std;
#define maxSize 5
typedef int type;
typedef struct sqQueue{
	type data[maxSize];
	int front,rear; 
}sqQueue;
//初始化
void init(sqQueue &queue){
	queue.front=0,queue.rear=0;
} 
//判队空
bool isEmpty(sqQueue &queue){
	//对头在对位的下一个位置时 说明队列已经满了  
	return queue.front==queue.rear;
} 
//判队满
bool isFull(sqQueue &queue){
	return (queue.rear+1)%maxSize==queue.front;
} 
//返回队列长度
int size(sqQueue &queue){
	return (queue.rear+maxSize-queue.front)%maxSize;
} 
//入队
int push(sqQueue &queue,type x){
	if(isFull(queue)) return -1;
	queue.data[queue.rear]=x;
	queue.rear=(queue.rear+1)%maxSize;
	return 1; 
} 
//出队
type pop(sqQueue &queue){
	if(isEmpty(queue)) return -1;
	
	type x=queue.data[queue.front];
	queue.front=(queue.front+1)%maxSize;
	return x;
} 
//发会队头元素
type  top(sqQueue &queue){
	if(isEmpty(queue)) return -1;
	return queue.data[queue.front];
}
int main(){
	sqQueue queue;
	init(queue);
	type x;
	cin>>x;
	while(x!=-1){
		push(queue,x);
		cin>>x;
	}
	cout<<isEmpty(queue)<<endl;
	cout<<size(queue)<<endl;
	cout<<isFull(queue)<<endl;
	
	cout<<top(queue)<<endl;
	cout<<"front"<<queue.front<<"rear"<<queue.rear<<endl;
	cout<<pop(queue)<<endl;
	cout<<"front"<<queue.front<<"rear"<<queue.rear<<endl; 
	push(queue,1);
	push(queue,2);
	push(queue,3);
	cout<<"front"<<queue.front<<"rear"<<queue.rear<<endl; 
	while(!isEmpty(queue))
		cout<<pop(queue)<<" ";
	cout<<endl;
	
	return 0;
}
