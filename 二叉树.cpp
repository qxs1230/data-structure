//二叉树的操作
#include<iostream>
#include<stdlib.h>
#include<cstring>
#include<algorithm>
#include<queue>
#include<vector>
#include<stack>
#include<cmath>
#define maxSize 100 //结点最大数量 
using namespace std;
typedef int type; 
typedef struct Node{
	type data;
	struct Node *left,*right;
}Node,*TreeNode;
//初始化 
void init(TreeNode &root){
	root=(TreeNode)malloc(sizeof(Node));
	root->left=NULL,root->right=NULL;
} 
// 构造树 
TreeNode  create(){
	TreeNode s,p[maxSize];
	int i,j;
	type x;
	cout<<"输入二叉树的层序遍历序列，空结点用 # 表示(最大节点数="<<maxSize<<") $ 表示输入结束:"<<endl;
	string str[maxSize];
	string t;
	int l=1;
	cin>>t;
	while(t.compare("$")!=0&&l<maxSize){
		str[l++]=t;
		cin>>t;
	}	
//	if(l==1&&str[0].compare("#")!=0) { //二叉树还是空的 
//		init(p[1]); 
//		//p[1]=NULL;
//	}
	for(int i=1;i<l;i++){
		if(str[i].compare("#")!=0) {
			s=(TreeNode)malloc(sizeof(Node));
			s->data=atoi(str[i].c_str());   //输入是 int时使用 
			//s->data=str[i].at(0);    //当输入是 char时使用 
			s->left=NULL; 
			s->right=NULL;
			p[i]=s;
			if(i!=1){
				j=i/2;  
			if(i%2==0)
				p[j]->left=s;
			else
				p[j]->right=s;	 
			}
		}
	}
	
	return p[1];
	
}
//递归先序遍历
void preorder(TreeNode &root){
	if(root){
		cout<<root->data<<" ";
		preorder(root->left);
		preorder(root->right);
	}
	
} 
//非递归先序遍历
void preorder_f(TreeNode &root){
	  if(root==NULL) return ;
	  stack<TreeNode> st;
	  vector<type> ans;
	  TreeNode s;
	  st.push(root);
	  while(!st.empty()){
	  	s=st.top();
	  	st.pop();
	  	ans.push_back(s->data);
	  	if(s->right)
	  		st.push(s->right);
	  	if(s->left)
	  		st.push(s->left);
	  	
	  }
	for(int i=0;i<ans.size();i++)
		cout<<ans[i]<<" ";
	cout<<endl;
} 

//递归中序遍历 
void inorder(TreeNode &root){
	if(root)
	{
		inorder(root->left);
		cout<<root->data<<" ";
		inorder(root->right);
	 } 
}
//非递归中序遍历
void inorder_f(TreeNode &root){
	if(root==NULL) return ;
	 stack<TreeNode> st;
	 vector<type> ans; 
	 TreeNode s=root;
	 while(s||!st.empty()){
	 	if(s){
	 		st.push(s);
	 		s=s->left;
		 }else{
		 	s=st.top();
		 	st.pop();
		 	ans.push_back(s->data);
		 	s=s->right;
		 }
	 }
	 for(int i=0;i<ans.size();i++)
	 	cout<<ans[i]<<" ";
	 cout<<endl;
	 
} 
//递归后续遍历
void postorder(TreeNode &root){
	if(root){
		postorder(root->left);
		postorder(root->right);
		cout<<root->data<<" ";
	}
} 
//非递归后序遍历
void postorder_f(TreeNode &root){
	if(root==NULL) return ;
	vector<type> ans;
	stack<TreeNode> s1;
	stack<TreeNode> s2;
	s1.push(root);
	TreeNode s;
	while(!s1.empty()){
		s=s1.top();
		s1.pop();
		s2.push(s);
		if(s->left)
			s1.push(s->left);
		if(s->right)
			s1.push(s->right);
	}
	while(!s2.empty()){
		ans.push_back(s2.top()->data);
		s2.pop();
	}
	for(int i=0;i<ans.size();i++)
		cout<<ans[i]<<" ";
	cout<<endl;
}
//层序遍历
void  bfs(TreeNode &root){
	if(root==NULL) return ;
	queue<TreeNode> q;
	vector<type> ans;
	q.push(root);
	while(!q.empty()){
		TreeNode s=q.front();
		q.pop();
		ans.push_back(s->data);
		if(s->left) 
			q.push(s->left);
		if(s->right)
			q.push(s->right);
	}
	for(int i=0;i<ans.size();i++)
		cout<<ans[i]<<" ";
	cout<<endl;
}
//层序遍历 
typedef struct node{
    TreeNode data;
}node;

void LevelorderTraversal( TreeNode root ){
        node a[100];
        int i=0,j=0;
        a[j++].data=root;
        while(i!=j){
            TreeNode p=a[i++].data;
            
            printf("%d ",p->data);
            if(root->left)
                   a[j++].data=p->left;
            if(root->right)
                   a[j++].data=p->right;
        }
}
//求树的深度 
int depth(TreeNode root){
	
	if(root==NULL) return 0;
	else{
	
	int l=depth(root->left); 
	int r=depth(root->right);
	return (l>r)?(l+1):(r+1);
}
}
// 非递归求树的深度
int f_depth(TreeNode root){
	queue<TreeNode> q;
	q.push(root);
	int depth=0;
	int width=0;//保存二叉树的宽度 
	while(!q.empty()){
		int index=q.size();    //相当于层次遍历 每次每次for循环之后 队列中只保存下一层的结点 
	//	width=max(index,width);
		 
		depth++;    
		for(int i=0;i<index;i++){
			TreeNode s=q.front();
			q.pop();
			
			if(s->left!=NULL)
				q.push(s->left);
			if(s->right!=NULL)
				q.push(s->right);
		}
	}
//	cout<<width<<endl;
	return depth;
} 
//由先序序列 和中序序列 构造二叉树 
TreeNode preincreat(type pre[],type in [],int p1,int p2,int i1,int i2){
	// pre 为先序遍历列表 p1 p2为先序遍历的第一个结点和 最后一个结点
	//in 为中序遍历列表 i1 i2为中序遍历的第一个结点和最后一个结点
	//  初始 p1=i1=1, p2=i2=n 
	TreeNode root=(TreeNode)malloc(sizeof(Node));
	root->data=pre[p1]; 							//根节点就是 先序遍历的第一个结点
	//找到中序遍历中根节点所在的位置
	int index=0;
	for(index =i1;in[index]!=root->data;index++);
	int llen=index-i1;			//左子树长度 
	int rlen=i2-index;			//右子树长度
	if(llen)
		root->left=preincreat(pre,in,p1+1,p1+llen,i1,i1+llen-1);
	else 
		root->left=NULL;
	if(rlen)
		root->right=preincreat(pre,in,p2-rlen+1,p2,i2-rlen+1,i2);
	else
		root->right=NULL;
	return root; 
} 

 
//由中序序列和后序序列  构造二叉树
TreeNode inpostcreat(type in[],type post[],int i1,int i2,int po1,int po2){
	 // post 为后序遍历序列
	 // i1=po1=1; i2=po2=n;
	 TreeNode root=(TreeNode)malloc(sizeof(Node));
	 //后序遍历的最后一个结点就是根节点
	 root->data=post[po2];
	 int index=0;
	 //找到中序遍历的更根节点所在的位置
	 for(index;root->data!=in[index];index++);
	 int llen=index-i1;   //左子树长度 
	 int rlen=i2-index;   //右子树长度 
	 if(llen)
	 	root->left=inpostcreat(in,post,i1,i1+llen-1,po1,po1+llen-1);
	 else
	 	root->left=NULL;
	 if(rlen)
	 	root->right=inpostcreat(in,post,i2-rlen+1,i2,po2-rlen,po2-1);
	 else
	 	root->right=NULL;
	return root;
} 


//判断一棵树 是不是完全二叉树
/**
		首先要明白完全二叉树的定义：
			在二叉树中 高度为h 除去最后一层外 每一层的结点数都是 2^(i-1) 也就是满节点
			并且最后一层的所有叶子结点都是从左到又 没有间隙的 
**/ 
 
bool judge(TreeNode root){
	if(root==NULL) return false;
	
	queue<TreeNode>	q;
	bool flag=false; 
	q.push(root);
	while(!q.empty()){
		TreeNode s=q.front();
		q.pop();
		if(s==NULL){
			flag=true;
			continue;
		}
		// 观察队列中的元素 会发现 空结点是连续的 若是不连续 就不是完全二叉树 
		if(flag) return false;
		q.push(s->left);
		q.push(s->right);
	}
	return true;
} 
// 交换结点的左右子树
void changelr(TreeNode &root){
	queue<TreeNode> q;
	q.push(root);
	while(!q.empty()){
		TreeNode n=q.front();
		q.pop();
		if(n->left!=NULL&&n->right!=NULL)
		{
			TreeNode l=n->left;
			TreeNode r=n->right;
			
			n->left=r;
			n->right=l;
			q.push(n->left);
			q.push(n->right);
		}
	}
} 
// 递归交换 结点左右子树
void swaplr(TreeNode &root){
	if(root){
		swaplr(root->left);
		swaplr(root->right);
		TreeNode temp=root->left;
		root->left=root->right;
		root->right=temp; 
	}
} 
// 讲中缀表达式的二叉树 输出为表达式
void TreetoExp(TreeNode root,int depth){
	if(root==NULL) return ;
	else if(root->left==NULL&&root->right==NULL)    //叶子节点 直接输出 操作数 
		 cout<<root->data<<" ";
	else{
		if(depth>1) cout<<"( ";  // 根节点之外的其他子树  遍历左子树之前输出左括号 
		TreetoExp(root->left,depth+1);
		cout<<root->data<<" ";				//中序遍历 
		TreetoExp(root->right,depth+1);  // 在右子树之后 输出 右括号  
		if(depth>1) cout<<") ";
	} 
} 

//求解一条从根根结点到叶子结点的   输出所有满足数值要求的路径 
//  lu不能使用引用传参， 
void dfs(TreeNode root,int targetnum,vector<int> lu,vector< vector<int> >  &li){
		if(root==NULL)
			return ;
		lu.push_back(root->data);
		if(targetnum==root->data&&(root->left==NULL&&root->right==NULL))
			{
				li.push_back(lu);
				lu.clear();
			}
			dfs(root->left,targetnum-root->data,lu,li);
			dfs(root->right,targetnum-root->data,lu,li);
			lu.pop_back();
} 
int main(){

	TreeNode root;	
	root=create();
//	cout<<"层序遍历:     ";
//	bfs(root);
//	cout<<"递归先序:     ";
	preorder(root);
	cout<<endl;
//	cout<<"非递归先序:   ";
//	preorder_f(root);
//	cout<<"递归中序:     ";
//	inorder(root);
//	cout<<endl;
//	cout<<"非递归中序:   ";
//	inorder_f(root);
//	cout<<"递归后序:     ";
//	postorder(root); 
//	cout<<endl;
//	cout<<"非递归后序遍历";
//	postorder_f(root);
	//cout<<"树的深度：    ";
	//cout<<f_depth(root)<<endl; 
	//LevelorderTraversal(root);
	//cout<<endl;
	
	type pre[9]={1,2,4,6,5,7,8,3,9};
	type in [9]={6,4,2,7,5,8,1,3,9};
	type post[]={6,4,7,8,5,2,9,3,1};
	//TreeNode newroot=preincreat(pre,in,0,8,0,8);
	//postorder(newroot);
	//TreeNode newroot=inpostcreat(in,post,0,8,0,8);
	//preorder(newroot);
	//cout<<endl;
	//cout<<judge(root)<<endl;
	//swaplr(root);
	//bfs(root);
	//TreetoExp(root,1);
	
	cout<<endl;
	vector<int> num;
	vector< vector<int> > li;

	dfs(root,22,num,li);

	for(int i=0;i<li.size();i++)
		{
			{
			for(int j=0;j<li[i].size();j++)
				cout<<li[i][j]<<" "; 
			}
		cout<<endl;
		}

	return 0;
}

