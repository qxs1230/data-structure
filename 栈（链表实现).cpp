// 链式栈的实现
#include<iostream>
#include<stdlib.h>
using namespace std;
typedef int type;
typedef struct LinkNode{
	type data;
	struct LinkNode *next;
}Node ,*Stack;
//初始化栈 
void init(Stack &stack){
	stack=(Stack)malloc(sizeof(Node));
	stack->next=NULL; 
} 
//判栈空 
bool isEmpty(Stack &stack){
	 return stack->next==NULL;
}
//返回栈的大小
int size(Stack stack){
	int len=0;
	Stack s=stack->next;
	while(s){
		len++;
		s=s->next;
	}
	return len;
} 
/**
	进栈操作  因为栈时常是在维护栈顶的元素  
	所以采用一个带头节点的 链表比较容易实现算法 
**/ 
void push(Stack &stack,type x){
	 Node *s=(Stack)malloc(sizeof(Node));
	 s->data=x;
	 s->next=stack->next;
	 stack->next=s; 
} 
//出栈操作
type pop(Stack &stack){
	if(isEmpty(stack)) return -1;
	Node *s=(Stack)malloc(sizeof(Node));
	s=stack->next;
	stack->next=s->next;
	int x=s->data;
	free(s);
	return x; 
} 
// 返回栈顶元素值
type top(Stack &stack){
	if(isEmpty(stack)) return -1;
	return stack->next->data;
} 
//销毁栈 
void destroy(Stack &stack){
	Stack p;
	
	while(stack){
		p=stack->next;
		free(stack);
		stack=p;
	}
} 

int main(){
	Stack stack;
	init(stack);
	type x;
	cin>>x;
	while(x!=-999){
		push(stack,x);
		cin>>x;
	}
	cout<<top(stack)<<endl;
	cout<<size(stack)<<endl;
	while(!isEmpty(stack)){
		cout<<pop(stack)<<" ";
	}
	cout<<endl;
	return 0;
}


