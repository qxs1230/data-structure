#include<iostream>
#include<stack>
#include<cstring>
#include<stdlib.h>
#include<stdio.h>

using namespace std;
const int N=40;
float option(float a,float b,char op){
	if(op=='+') return a+b;
	else if(op=='-') return a-b;
	else if(op=='*') return a*b;
	else if(op=='/') return a/b;
}
int main(){
	stack<float> st;
   string s;
   getline(cin,s);
   cout<<s<<endl;
   for(int i=s.size()-1;i>=0;i--){
   		string num=" ";
   		if(s[i]=='+'||s[i]=='-'||s[i]=='*'||s[i]=='/'){
		   float t1=st.top();
		   st.pop();
		   float t2=st.top();
		   st.pop();
		   if(s[i]=='/'&&t2==0.0){
		   	 cout<<"ERROR"<<endl;
		   	 return 0;
		   }
		   st.push(option(t1,t2,s[i]));
		   i--;
		}
		else{
			while(i>=0&&s[i]!=' '){
				num+=s[i];
				i--;
			}
			st.push(atof(num.c_str()));
		}
   			
   		}
   	printf("%.1f",st.top());
   		
   
	
}
