//带头节点的单链表操作
#include<iostream>
#include<stdlib.h>
using namespace std; 
typedef int type;
typedef struct node{
	type data;
	struct node *next;
} Node,*LinkNode; 
//初始化头节点 
void init(LinkNode &head)
{
	head=(LinkNode)malloc(sizeof(Node));
	head->next=NULL;
 } 
//头插法
LinkNode createR(){
	type x;
	cin>>x;
	LinkNode head,s;
	init(head);
	while(x!=-1){
		LinkNode s=(LinkNode)malloc(sizeof(Node));
		s->data=x;
		s->next=head->next;
		head->next=s;
		cin>>x;
		}
	
	return head; 
}
//尾插法
LinkNode createF(){
	LinkNode head,s,p;
	init(head);
	p=head;
	type x;
	cin>>x;
	while(x!=-1){
		s=(LinkNode) malloc(sizeof(Node));
		s->data=x;
		s->next=NULL;
		p->next=s;
		p=s; 
		cin>>x;
	}
	return head;
} 
//判断链表是否为空
int  isEmpty(LinkNode head){
	return head->next==NULL;
}
//销毁整个链表
void destory(LinkNode &head){
	LinkNode p;
	while(head){
		p=head->next;
		free(head);
		head=p;
	}

} 
//返回链表长度
int length(LinkNode head){
	if(isEmpty(head)) return 0;
	LinkNode p=head->next;
	int t=0;
	while(p){
		t++;
		p=p->next;
	}
	return t; 
} 
//在指定序号插入元素
int insertElem(LinkNode head,int p,type x){
	if(isEmpty(head)||p>length(head)||p<0) return 0;
	LinkNode q=head->next;
	for(int i=0;i<p-1;i++)
			q=q->next;
	LinkNode s=(LinkNode) malloc(sizeof(Node));
	s->data=x;
	s->next=q->next;
	q->next=s;
	return 1;
} 
//查找指定位置的元素并返回
type getElem(LinkNode head,int p){
	if(isEmpty(head)||p>=length(head)||p<0) return -1;
	LinkNode q=head->next;
	int i=0;
	while(p&&i!=p)
	{
		q=q->next;
		i++;
	}
	return q->data;
} 
//找指定元素的位置
int findElem(LinkNode head,type x){
	if(isEmpty(head)) return -1;
	LinkNode p=head->next;
	int i=0;
	while(p&&p->data!=x){
		i++;
		p=p->next;
	}
	if(p==NULL) return -1;
	return i;
}
//删除链表中出现的所有 元素 x
int deleteElem(LinkNode head,type x){
	if(isEmpty(head)) return -1;
	LinkNode p=head->next;
	LinkNode q=head;
	LinkNode l;
	while(p->next){
		if(p->data==x){
			l=p->next;
			p->data=l->data;
			p->next=l->next;
			free(l); 
		}else{
			p=p->next;
			q=q->next;
		}
		
	}
	
	if(p->data==x){  //应对需要删除的元素是最后一个结点的情况 
		
		q->next=NULL;
		free(p);
	}
	return 1;
} 
//删除指定位置的结点 
int deleteElem_p(LinkNode head,int p){
	if(isEmpty(head)||p>=length(head)||p<0) return -1;
	LinkNode q=head->next;
	LinkNode l=head;
	int i=0;
	while(q->next&&i!=p){
		q=q->next;
		l=l->next;
		i++;
	}
	l->next=q->next;
	free(q);
	return 1;
} 
//就地逆转带头节点的链表
LinkNode reverse(LinkNode head){
	LinkNode newhead;
	init(newhead);
	LinkNode p=newhead,q=head->next,l;
	while(q){
		l=q->next;
		q->next=newhead->next;
		newhead->next=q;
		q=l;
	}
	return newhead;
} 
//从尾到头输出打印链表
void reverse_print(LinkNode head){
	if(head->next){
		reverse_print(head->next);
	}
	if(head) cout<<head->data<<" ";
} 

//返回链表的倒数第k个元素  最后一个为倒数第一个
void seach_k(LinkNode head,int k){
	
	//然后先走k步 
	//另外一个链表从头开始走
	//当前面一个链表走到尾的时候 他刚好到第k个位置
	LinkNode p=head->next;
	LinkNode q=head->next;
	for(int i=0;i<k;i++)
		p=p->next;
	while(p&&q){
		p=p->next;
		q=q->next;
	}
	cout<<q->data<<endl;
} 
// 链表增序排序
void sort(LinkNode head){
	if(isEmpty(head)) return ;
	//读到数组 在排序 当排序算法复杂度为nlogn的时候 总的算法复杂度就是nlogn
	 LinkNode p=head->next,pre;  
	 LinkNode r=p->next; //r保存 p后序结点指针 
	 p->next=NULL;		//构造只有一个结点的有序链表 
	 p=r;
	 while(p){
	 	r=p->next;   //保存p后序结点指针 
	 	pre=head;
	 	while(pre->next&&pre->next->data<p->data)    //O(n^2)的复杂度 
	 		pre=pre->next;   //找到链表中 第一个比p->data大的结点 
	 	p->next=pre->next;   // 将其插入到 有序链表中 
	 	pre->next=p;         
	 	p=r;				//查找链表剩余的结点 
	 } 
} 
//打印链表 
void print(LinkNode head){
	if(isEmpty(head)) {
		cout<<-1<<endl;
		return ;
	}
 	LinkNode p=head->next;
	while(p)
	{
		cout<<p->data<<" ";
		p=p->next;
	}
	cout<<endl;
}
int main(){
	LinkNode head=createF();
	print(head);
 	//LinkNode head1=createF();
	//print(head1);
//	cout<<isEmpty(head)<<endl;
//	insertElem(head,2,18);
//	
//	print(head);
//	cout<<length(head)<<endl;
//	cout<<findElem(head,2)<<endl;
//	cout<<getElem(head,4)<<endl;
//	cout<<deleteElem(head,2)<<endl;
//	print(head);
//	deleteElem_p(head,3);
//	print(head);
	//destory(head);
	//LinkNode newhead=reverse(head);
//	reverse_print(head->next);
	//sort(head);
	//print(head);
	//print(newhead);
	seach_k(head,4);
	return 0;
}
