//队列
#include<iostream>
using namespace std;
typedef int type;
#define maxSize 100
typedef struct Queue{
	type data[maxSize];
	int front,rear;
}Queue;
//初始化
void init(Queue &queue){
	queue.front=0;
	queue.rear=0;
} 
//判对空
bool isEmpty(Queue &queue){
	return queue.front==queue.rear;
} 
//判对满
bool isFull(Queue &queue){
	//这种判断顺序队列是否满的方式是不准确的  
	//可能队列中还有存放元素的位置 造成一种“假溢出”的现象 
	return queue.rear==maxSize;
}
//入队操作
void push(Queue &queue,type x){
	if(!isFull(queue)) queue.data[queue.rear++]=x;
} 
//返回对头元素
type top(Queue &queue){
	if(isEmpty(queue)) return -1;
	return  queue.data[queue.front];
}  
//出队操作
type pop(Queue &queue){
	if(isEmpty(queue)) return -1;
	return queue.data[--queue.rear];
} 
//返回队列元素数目
int size(Queue &queue){
	return queue.rear-queue.front;
} 
//清空队列
void clear(Queue &queue){
	init(queue);  //不知道科不科学 
} 
int main(){
	Queue queue;
	init(queue);
	type x;
	cin>>x;
	while(x!=-1){
		push(queue,x);
		cin>>x;
	}
	cout<<size(queue)<<endl;
	cout<<isEmpty(queue)<<endl;
	cout<<top(queue)<<endl;
	cout<<isFull(queue)<<endl;
	cout<<"rear"<<queue.rear<<endl;
	while(!isEmpty(queue)){
		cout<<pop(queue)<<" ";
	}
	clear(queue);
	while(!isEmpty(queue)){
		cout<<pop(queue)<<" ";
	}
	return 0;
} 
