#include <stdio.h>
#include <stdlib.h>

#define ERROR NULL
//typedef enum {false, true} bool;
typedef int ElementType;
typedef struct LNode *PtrToLNode;
struct LNode {
    ElementType Data;
    PtrToLNode Next;
};
typedef PtrToLNode Position;
typedef PtrToLNode List;

List MakeEmpty(); 
Position Find( List L, ElementType X );
bool Insert( List L, ElementType X, Position P );
bool Delete( List L, Position P );

int main()
{
    List L;
    ElementType X;
    Position P;
    int N;
    bool flag;

    L = MakeEmpty();
   
    scanf("%d", &N);
    while ( N-- ) {
        scanf("%d", &X);
        flag = Insert(L, X, L->Next);
        if ( flag==false ) printf("Wrong Answer\n");
    }
    scanf("%d", &N);
    while ( N-- ) {
        scanf("%d", &X);
        P = Find(L, X);
        if ( P == ERROR )
            printf("Finding Error: %d is not in.\n", X);
        else {
            flag = Delete(L, P);
            printf("%d is found and deleted.\n", X);
            if ( flag==false )
                printf("Wrong Answer.\n");
        }
    }
    flag = Insert(L, X, NULL);
    if ( flag==false ) printf("Wrong Answer\n");
    else
        printf("%d is inserted as the last element.\n", X);
    P = (Position)malloc(sizeof(struct LNode));
    flag = Insert(L, X, P);
    if ( flag==true ) printf("Wrong Answer\n");
    flag = Delete(L, P);
    if ( flag==true ) printf("Wrong Answer\n");
    for ( P=L->Next; P; P = P->Next ) printf("%d ", P->Data);
    return 0;
}
/* 你的代码将被嵌在这里 */
int length(List L){
    List p=L->Next;
    int i=0;
    while(p){
        i++;
        p=p->Next;
    }
    return i;
}
List MakeEmpty(){
    List head=(List)malloc(sizeof(PtrToLNode));
    head->Next=NULL;
    return head;
}
Position Find(List L, ElementType X)
{
	Position pre = L;
	while (pre&&pre->Data != X)
	{
		pre = pre->Next;
	}
	if (pre) return pre;
	else return false;
}

bool Insert(List L, ElementType X, Position P)
{
	Position pre = L,tmp;
	tmp = (List)malloc(sizeof(struct LNode));
	tmp->Data = X;
	tmp->Next = NULL;
	if (pre == P) 
	{
		tmp->Next = L;
		L = tmp;
		return true;
	}
	while (pre->Next!=NULL&&pre->Next!=P)
	{
		pre = pre->Next;
	}
	
	if (pre->Next==P) 
	{
		tmp->Next = pre->Next;
		pre->Next = tmp;
		return true;
	}
	else
	{
		printf("Wrong Position for Insertion\n");
		return false;
	}
}
bool Delete(List L, Position P)
{
	Position pre = L,p=L, tmp;
	tmp = (List)malloc(sizeof(struct LNode));
	tmp->Next = NULL;
	if (P == L)
	{
		pre = pre->Next;
		return true;
	}
	else
	{
		while (pre->Next!=NULL&&pre->Next != P)
		{
			pre = pre->Next;
		}
		if (pre->Next==P)
		{
			tmp = pre->Next;
			pre->Next = tmp->Next;
			return true;
		}
		else
		{
			printf("Wrong Position for Deletion\n");
			return false;
		}
	}
}

